#include <WiFi.h>
#include <i18n.h>
#include <IRac.h>
#include <IRrecv.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRtext.h>
#include <IRtimer.h>
#include <ir_Daikin.h>
#include <Arduino.h>
#include <PubSubClient.h>

const uint16_t kIrLed = 13;  // ESP8266 GPIO pin to use.
IRDaikinESP ac(kIrLed);


const char* ssid = "SmartiAM-UniFi"; //WiFi SSID
const char* password = "12345678";   // WiFi Password
const char* mqtt_server = "192.168.2.22";
int BROKER_PORT = 1883;


WiFiClient espClient;
PubSubClient client(espClient);

String acdaikin;
String strTopic;
String strPayload;

void setup()
{

  Serial.begin(115200);
  ac.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);


}



void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  strTopic = String((char*)topic);
  if (strTopic == "ha/switch1")
  {
    acdaikin = String((char*)payload);
    if (acdaikin == "ON")
    {
      ac.on();
      ac.setFan(3);
      ac.setTemp(20);
      ac.setMode(kDaikinCool);
      ac.setSwingVertical(true);
      ac.setSwingHorizontal(false);

      ac.send();
      Serial.println("AC_ON");
    }
    acdaikin = String((char*)payload);
    if (acdaikin == "OFF")
    {
      Serial.println("OFF");

      ac.off();
      ac.send();
    }
  }
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.subscribe("ha/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void loop()
{
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
